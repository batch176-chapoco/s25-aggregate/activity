//ACTIVITY : (AGGREGATE-176)
//TOTAL NUMBER OF ITEMS SUPPLIED BY RED FARMS INC.
db.fruits.aggregate([

    { $match: { supplier: "Red Farms Inc." } },
    { $count: "numberofItemsbyRedFarms" }

])

//TOTAL NUMBER OF ITEMS WITH PRICE GREATER THAN 50
db.fruits.aggregate([

    { $match: { price: { $gt: 50 } } },
    { $count: "itemsGT50" }

])

//AVERAGE PRICE OF ALL FRUITS THAT ARE ON SALE PER SUPPLIER
db.fruits.aggregate([

    { $match: { onSale: true } },
    { $group: { _id: "$supplier", avgPrice: { $avg: "$price" } } }
])

//HIGHEST PRICE OF FRUITS THAT ARE ON SALE PER SUPPLIER
db.fruits.aggregate([

    { $match: { onSale: true } },
    { $group: { _id: "$supplier", maxPrice: { $max: "$price" } } }
])
//LOWEST PRICE OF FRUITS THAT ARE ON SALE PER SUPPLIER
db.fruits.aggregate([

    { $match: { onSale: true } },
    { $group: { _id: "$supplier", minPrice: { $min: "$price" } } }
])